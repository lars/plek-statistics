const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' },
      { test: /\.(png)$/, use: ['file-loader'] }
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: 'src/index.html'
      },
    ])
  ],
};