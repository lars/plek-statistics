# Changelog
The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
* Integrate basic frame
* Add menu component
* Add breadcrumb component
* Add notifications component
* Add export button component
* Add datepicker component
* Add daterange component
* Add platform type component
* Implement Chartjs library with basic chart example

## [0.0.2] 2018-05-09
### Added
* Add header component
* Add styled components package to manage styling
* Add jest for test driven development
* Implement Redux into React
* Add linter to maintain code style
* Added hot reloading to Webpack configuration

### Changed
* Fix versions in package.json
* Splitted Webpack configuration in three files: dev, prod & common

## [0.0.1] 2018-05-07
### Added
* Add Webpack and Babel configuration to build React supporting JSX syntax 
* Add basic React setup
* Add CHANGELOG to keep track of notable changes in the project
* Add README to document installation instructions and other valuable information about the project