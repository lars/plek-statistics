import React from 'react'
import { connect } from 'react-redux'
import Header from './../components/Header'

const mapStateToProps = (state) => {
  console.log(state)
}

const HeaderContainer = () => (
  <Header />
)

export default connect(mapStateToProps)(HeaderContainer)