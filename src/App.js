import React from 'react'
import Header from './containers/Header'
import Menu from './components/Menu'
import Main from './components/Main'

const App = () =>  (
  <div>
    <Header />
    <Menu />
    <Main />
  </div>
)

export default App
