import React from 'react'
import { Switch, Route } from 'react-router-dom'
import styled from 'styled-components'
import OverviewPage from './Pages/Overview'
import ReportPage from './Pages/Report'

const Wrapper = styled.main`
  margin-left: 64px
  padding: 20px

  p {
    margin: 0
  }
`

const Main = () => (
  <Wrapper>
    <Switch>
      <Route exact path='/' component={OverviewPage}/>
      <Route path='/report' component={ReportPage}/>
    </Switch>
  </Wrapper>
)

export default Main