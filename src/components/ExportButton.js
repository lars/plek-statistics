import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.button`
  background: #E6E6E6
  border: 0
  border-radius: 4px
  color: #333
  font-size: 14px
  font-weight: 600
  padding: 8px 16px
  text-align: center
`

const ContentHeader = () => (
  <Wrapper>
    Export report
  </Wrapper>
)

export default ContentHeader