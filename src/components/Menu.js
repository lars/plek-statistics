import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import MenuIconOverview from './../assets/img/menu-icon-overview.png'
import MenuIconReport from './../assets/img/menu-icon-report.png'

const Wrapper = styled.nav`
  background: #2A2E35
  position: fixed
  top: 54px
  left: 0
  height: 100%
  width: 64px

  ul {
    list-style: none
    margin: 0
    padding-left: 0
  }

  a {
    display: block
    padding: 10px 20px
  }

  img {
    width: 24px
  }
`

const Menu = () => (
  <Wrapper>
    <ul>
      <li><Link to='/'><img src={MenuIconOverview} /></Link></li>
      <li><Link to='/report'><img src={MenuIconReport} /></Link></li>
    </ul>
  </Wrapper>
)

export default Menu