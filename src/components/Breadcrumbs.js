import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.ul`
  margin: 0
`

const Breadcrumbs = () => (
  <Wrapper>
    <li>Statistics</li>
    <li>Performance</li>
    <li>Report</li>
  </Wrapper>
)

export default Breadcrumbs