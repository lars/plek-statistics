import React from 'react'
import styled from 'styled-components'
import ExportButton from './ExportButton'

const ContentHeader = styled.header`
  background: white;
  margin-bottom: 20px
  padding: 20px;

  h1 {
    font-size: 32px
    font-weight: 600
    line-height: 1.1
    margin: 0 0 10px 0
  }

  div {
    display: flex
  }

  p {
    flex: 1
    line-height: 1.1
    margin: 0
  }
`


const Overview = () => (
  <div>
    <ContentHeader>
      <h1>Overview</h1>
      <div>
        <p>Some text</p>
        <ExportButton />
      </div>
    </ContentHeader>
    <p>Overview</p>
  </div>
)

export default Overview