import React from 'react'
import styled from 'styled-components'
import Breadcrumbs from './Breadcrumbs'
import Notifications from './Notifications'
import Logo from './../assets/img/plek_logo.png'

const Wrapper = styled.section`
  background: white
  display: flex

  div {
    display: flex
    align-items: center
    flex: 1
    line-height: 54px
    padding-right: 20px
    padding-left: 20px
  }

  img {
    width: 24px
  }
  
  > a {
    background: #181B1F
    display: flex
    align-items: center
    justify-content: center
    width: 64px
  }

  ul {
    flex: 1
    list-style: none
    margin: 0
    padding-left: 0
  }

  li {
    float: left
  }
`

const Header = () => (
  <Wrapper>
    <a href=""><img src={Logo} /></a>
    <div>
      <Breadcrumbs />
      <Notifications />
    </div>
  </Wrapper>
)

export default Header