import React from 'react'
import styled from 'styled-components'
import NotificationsIcon from './../assets/img/notifications_icon.png'

const Wrapper = styled.img`
  margin: 0
`

const Notifications = () => (
  <Wrapper src={NotificationsIcon} />
)

export default Notifications