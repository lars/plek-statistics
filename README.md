## Getting started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Installation
Clone the repository:
```bash
$ git clone git@git.ilumy.com:lars/plek-statistics.git
```

Install dependencies required to run the project:
```bash
$ yarn install
```

Build the project in development mode:
```bash
$ yarn build
```

You can develop and test the project by opening the `dist/index.html` in your browser

## Running tests
We use `jest` to run the test. You can start the tests the following way:
```bash
$ yarn test
```

## Tech stack
* React - Web framework
* Redux
* Webpack
* Babel
* ESLint - Code style checking
* Jest - To automate tests
* Styled components - To structure styling per component
* Browser router - Routing

## Contributing
Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests.